#pragma once
#include <d3dx9.h>

const D3DCOLOR WHITE   = D3DCOLOR_XRGB(255, 255, 255); // 0xffffffff
const D3DCOLOR BLACK   = D3DCOLOR_XRGB(0, 0, 0);       // 0xff000000
const D3DCOLOR RED     = D3DCOLOR_XRGB(255, 0, 0);     // 0xffff0000
const D3DCOLOR GREEN   = D3DCOLOR_XRGB(0, 255, 0);     // 0xff00ff00
const D3DCOLOR BLUE    = D3DCOLOR_XRGB(0, 0, 255);     // 0xff0000ff
const D3DCOLOR YELLOW  = D3DCOLOR_XRGB(255, 255, 0);   // 0xffffff00
const D3DCOLOR CYAN    = D3DCOLOR_XRGB(0, 255, 255);   // 0xff00ffff
const D3DCOLOR MAGENTA = D3DCOLOR_XRGB(255, 0, 255);   // 0xffff00ff
const D3DCOLOR GREY    = D3DCOLOR_XRGB(96, 96, 96);	   // 0xFFFFFFFF
const D3DCOLOR LBLUE   = D3DCOLOR_XRGB(0, 255, 255);
const D3DCOLOR LIMEG   = D3DCOLOR_XRGB(128, 255, 0);

void InitVertexDeclarations(IDirect3DDevice9* gd3dDevice);//does this ever get called?
void DestroyVertexDeclarations();

struct VertexP
{
	VertexP():Position(0.0f, 0.0f, 0.0f){}
	VertexP(float x, float y, float z):Position(x,y,z){}
	VertexP(const D3DXVECTOR3& vectorP):Position(vectorP){}

	D3DXVECTOR3 Position;
	static IDirect3DVertexDeclaration9* Decleration;
};

struct VertexC
{
	VertexC():Position(0.0f, 0.0f, 0.0f),Color(0x00000000){}
	VertexC(float x, float y, float z, D3DCOLOR color):Position(x,y,z), Color(color){}
	VertexC(const D3DXVECTOR3& vectorP, D3DCOLOR color):Position(vectorP),Color(color){}

	D3DXVECTOR3 Position;
	D3DCOLOR    Color;
	static IDirect3DVertexDeclaration9* Decleration;
};

struct VertexPN
{
	VertexPN():Position(0.0f,0.0f,0.0f){}
	VertexPN(float x, float y, float z):Position(x,y,z){}
	VertexPN(const D3DXVECTOR3& vectorP, D3DXVECTOR3 vectorN):Position(vectorP), Normal(vectorN){}

	D3DXVECTOR3 Position, Normal;
	static IDirect3DVertexDeclaration9* Decleration;
};

struct VertexPNT
{
	VertexPNT():Position(0.0f, 0.0f, 0.0f), Normal(0.0f, 0.0f, 0.0f), Texture(0.0f, 0.0f){}
	VertexPNT(float x, float y, float z, float nomrX, float normY, float normZ, float textU, float textV):Position(x,y,z), Normal(nomrX,normY,normZ), Texture(textU,textV){}
	VertexPNT(const D3DXVECTOR3& vectorP, const D3DXVECTOR3& vectorN, const D3DXVECTOR2& textUV):Position(vectorP),Normal(vectorN), Texture(textUV){}

	D3DXVECTOR3 Position, Normal;
	D3DXVECTOR2 Texture;

	static IDirect3DVertexDeclaration9* Decleration;
};

struct Particle
{
	D3DXVECTOR3 initialPos;
	D3DXVECTOR3 initialVelocity;
	float       initialSize; // In pixels.
	float       initialTime;
	float       lifeTime;
	float       mass;
	D3DCOLOR    initialColor;

	static IDirect3DVertexDeclaration9* Decl;
};