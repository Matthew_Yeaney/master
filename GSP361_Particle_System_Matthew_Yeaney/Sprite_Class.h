#pragma once

#include "d3dApp.h"


class Sprite_Class
{
private:
	D3DVECTOR m_Position;
	D3DVECTOR m_Velocity;
	D3DMATRIX m_Rotate;

public:
	ID3DXSprite *Sprite;			//creates the sprite

	Sprite_Class(void);
	~Sprite_Class(void);

	//Accessors
	D3DVECTOR getPos(){return m_Position;}
	D3DVECTOR getVel(){return m_Velocity;}
	D3DMATRIX getMatrix(){return m_Rotate;}

	//Mutators
	void setPos(D3DVECTOR pos){m_Position = pos;}
	void setVel(D3DVECTOR vel){m_Velocity = vel;}
	void setMat(D3DMATRIX rot){m_Rotate = rot;}
	
	//Methods
	void Update(float dt);



	

};

