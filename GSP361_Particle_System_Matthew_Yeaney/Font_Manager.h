#pragma once
#include <map>
#include <vector>
#include <string>
#include <d3d9.h>
#include <d3dx9.h>
#include <DxErr.h>


class Font_Manager
{
private:
	std::vector<ID3DXFont*> m_VectorOfFonts;
	std::map<std::string,ID3DXFont*> m_MapOfFonts;

public:
	Font_Manager(void);
	~Font_Manager(void);

	//registers the font into the map and vector
	bool Register_Font(std::string name, D3DXFONT_DESC Font);

	//finds the font in the map
	ID3DXFont* get_Font(std::string name);

	void onLostDevice();
	void onResetDevice();

	// register font ( string name, d3d font desc ) return bool 
	/*
		{
			auto retVal = map.find(name);
			if(retval!=map.end())
			{
				map.insert(std::make_pair(name,font));
			}
			else
			{
				name already in use return false meaning the function failed
			}
		}
		id3dxfont* get registered font(name);
		onlost()
		on rest();
	*/
};

