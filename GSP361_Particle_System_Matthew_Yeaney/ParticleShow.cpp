#include "ParticleShow.h"
#include "Resource_Manager.h"
#include "Particle_Manager.h"
#include "State_Manager.h"
#include "Button_Manager.h"
#include "Font_Manager.h"
#include "PSystem.h"
#include "PSprinkler.h"
#include "Sprite_Button.h"
#include "Sprite_Slider.h"
#include "DirectInput.h"
#include "d3dApp.h"


ParticleShow::ParticleShow(void)
{
	mGfx_Stats = new Gfx_Stats();

	Button_Desc tButtonDEsc;
	tButtonDEsc.bFont = "font1";					//the font used for the button
	tButtonDEsc.label = "Editing Area";					//Label that is written on the Button
	tButtonDEsc.Posit = D3DXVECTOR3(700,25,0);		//sets the buttons position on the screen
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);			//sets the initial velocity of the button
	tButtonDEsc.Center = D3DXVECTOR3(64,16,0);		//sets the center of the button for the texture
	tButtonDEsc.tString = "Slider_BackGround.png";	//sets the Texture of the button
	tButtonDEsc.click = false;						//sets the button clicked to false so it can be clicked
	tButtonDEsc.bEffect = [&]()						//Lambda that makes the button work.
	{
		//gd3dApp->StateManaged()->switchState("LifeTime");
	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("Edit_Area_Button",&tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("Edit_Area_Button"));

	tButtonDEsc.label = "Quit";
	tButtonDEsc.Posit = D3DXVECTOR3(700,550,0);
	tButtonDEsc.bEffect = [&]()
	{
		exit(1);
	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("Quit",&tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("Quit"));

	//==================== Sliders to manipulate the Particles ==========================================================
	//Sets the Lifetime for the Particle

	tButtonDEsc.label = "LifeTime";
	tButtonDEsc.Posit = D3DXVECTOR3(700, 75,0);
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	tButtonDEsc.Center = D3DXVECTOR3(64, 16, 0);
	tButtonDEsc.tString = "Slider_BackGround.png";
	tButtonDEsc.click = false;
	tButtonDEsc.bEffect = [&]()
	{

	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("LifeTimeButton", &tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("LifeTimeButton"));

	//sets the arrow for the slider to increase it
	tButtonDEsc.label = "";
	tButtonDEsc.Posit = D3DXVECTOR3(783, 75, 0);
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	tButtonDEsc.Center = D3DXVECTOR3(16, 16,0);
	tButtonDEsc.tString = "Right_Arrow.png";
	tButtonDEsc.click = false;
	tButtonDEsc.bEffect = [&]()
	{
		if(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider1")->get_S_Pos() < 1)
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider1")->set_S_Val(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider1")->get_S_Pos() + .1);
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider1")->set_Slider_Pos(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider1")->get_Slider_Pos() + D3DXVECTOR3(+10,0,0));
		}
		else
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider1")->set_S_Val(1);
		}
	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("LifeExtendButton", &tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("LifeExtendButton"));

	//sets the left arrow for the slider to decrease it
	tButtonDEsc.label = "";
	tButtonDEsc.Posit = D3DXVECTOR3(615, 75, 0);
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	tButtonDEsc.Center = D3DXVECTOR3(16, 16,0);
	tButtonDEsc.tString = "Left_Arrow.png";
	tButtonDEsc.click = false;
	tButtonDEsc.bEffect = [&]()
	{
		if(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider1")->get_S_Pos() > 0)
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider1")->set_S_Val(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider1")->get_S_Pos() - .1);
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider1")->set_Slider_Pos(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider1")->get_Slider_Pos() + D3DXVECTOR3(-10,0,0));
		}
		else
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider1")->set_S_Val(0);
		}
	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("LifeDecendButton", &tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("LifeDecendButton"));

	//================================== Creates the Slider for LifeTime =================================================================
	//creates the slider.
	Slider_Desc sliderDesc;
	sliderDesc.m_sPosit = D3DXVECTOR3(700,75,1);
	sliderDesc.m_sVeloc = D3DXVECTOR3(0,0,0);
	sliderDesc.m_sCenter = D3DXVECTOR3(64,16,0);
	sliderDesc.backImage = "Slider_BackGround.png";
	sliderDesc.sPos = D3DXVECTOR3(751,75,0);
	sliderDesc.foreImage = "Slider_Slider.png";
	sliderDesc.maxX = 1;
	sliderDesc.minX = 0;
	sliderDesc.SliderVal = .5;
	sliderDesc.click = false;
	sliderDesc.sEffect = [&]()
	{

	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Slider("Slider1", &sliderDesc);
	mEditSliders.push_back((gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider1")));

	//============================================== SIZE ==========================================================================
	//Size of the Particle
	tButtonDEsc.label = "Size";
	tButtonDEsc.Posit = D3DXVECTOR3(700, 115,0);
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	tButtonDEsc.Center = D3DXVECTOR3(64, 16, 0);
	tButtonDEsc.tString = "Slider_BackGround.png";
	tButtonDEsc.click = false;
	tButtonDEsc.bEffect = [&]()
	{

	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("SizeButton", &tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("SizeButton"));

	//sets the arrow for the slider to increase it
	tButtonDEsc.label = "";
	tButtonDEsc.Posit = D3DXVECTOR3(783, 115, 0);
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	tButtonDEsc.Center = D3DXVECTOR3(16, 16,0);
	tButtonDEsc.tString = "Right_Arrow.png";
	tButtonDEsc.click = false;
	tButtonDEsc.bEffect = [&]()
	{
		if(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider2")->get_S_Pos() < 1)
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider2")->set_S_Val(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider2")->get_S_Pos() + .1);
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider2")->set_Slider_Pos(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider2")->get_Slider_Pos() + D3DXVECTOR3(+10,0,0));
		}
		else
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider2")->set_S_Val(1);
		}
	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("SizeUpButton", &tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("SizeUpButton"));

	//sets the left arrow for the slider to decrease it
	tButtonDEsc.label = "";
	tButtonDEsc.Posit = D3DXVECTOR3(615, 115, 0);
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	tButtonDEsc.Center = D3DXVECTOR3(16, 16,0);
	tButtonDEsc.tString = "Left_Arrow.png";
	tButtonDEsc.click = false;
	tButtonDEsc.bEffect = [&]()
	{
		if(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider2")->get_S_Pos() > 0)
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider2")->set_S_Val(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider2")->get_S_Pos() - .1);
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider2")->set_Slider_Pos(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider2")->get_Slider_Pos() + D3DXVECTOR3(-10,0,0));
		}
		else
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider2")->set_S_Val(0);
		}
	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("SizeDownButton", &tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("SizeDownButton"));

	//================================== Creates the Slider for Size =================================================================
	//creates the slider.
	sliderDesc.m_sPosit = D3DXVECTOR3(700,115,1);
	sliderDesc.m_sVeloc = D3DXVECTOR3(0,0,0);
	sliderDesc.m_sCenter = D3DXVECTOR3(64,16,0);
	sliderDesc.backImage = "Slider_BackGround.png";
	sliderDesc.sPos = D3DXVECTOR3(751,115,0);
	sliderDesc.foreImage = "Slider_Slider.png";
	sliderDesc.maxX = 1;
	sliderDesc.minX = 0;
	sliderDesc.SliderVal = .5;
	sliderDesc.click = false;
	sliderDesc.sEffect = [&]()
	{

	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Slider("Slider2", &sliderDesc);
	mEditSliders.push_back((gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider2")));

	//==============================================  Mass ========================================================================
	//Size of the Particle
	tButtonDEsc.label = "Mass";
	tButtonDEsc.Posit = D3DXVECTOR3(700, 155,0);
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	tButtonDEsc.Center = D3DXVECTOR3(64, 16, 0);
	tButtonDEsc.tString = "Slider_BackGround.png";
	tButtonDEsc.click = false;
	tButtonDEsc.bEffect = [&]()
	{

	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("MassButton", &tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("MassButton"));

	//sets the arrow for the slider to increase it
	tButtonDEsc.label = "";
	tButtonDEsc.Posit = D3DXVECTOR3(783, 155, 0);
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	tButtonDEsc.Center = D3DXVECTOR3(16, 16,0);
	tButtonDEsc.tString = "Right_Arrow.png";
	tButtonDEsc.click = false;
	tButtonDEsc.bEffect = [&]()
	{
		if(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider3")->get_S_Pos() < 1)
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider3")->set_S_Val(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider3")->get_S_Pos() + .1);
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider3")->set_Slider_Pos(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider3")->get_Slider_Pos() + D3DXVECTOR3(+10,0,0));
		}
		else
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider3")->set_S_Val(1);
		}
	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("MassUpButton", &tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("MassUpButton"));

	//sets the left arrow for the slider to decrease it
	tButtonDEsc.label = "";
	tButtonDEsc.Posit = D3DXVECTOR3(615, 155, 0);
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	tButtonDEsc.Center = D3DXVECTOR3(16, 16,0);
	tButtonDEsc.tString = "Left_Arrow.png";
	tButtonDEsc.click = false;
	tButtonDEsc.bEffect = [&]()
	{
		if(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider3")->get_S_Pos() > 0)
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider3")->set_S_Val(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider3")->get_S_Pos() - .1);
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider3")->set_Slider_Pos(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider3")->get_Slider_Pos() + D3DXVECTOR3(-10,0,0));
		}
		else
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider3")->set_S_Val(0);
		}
	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("MassDownButton", &tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("MassDownButton"));

	//================================== Creates the Slider for Mass =================================================================
	//creates the slider.
	sliderDesc.m_sPosit = D3DXVECTOR3(700,155,1);
	sliderDesc.m_sVeloc = D3DXVECTOR3(0,0,0);
	sliderDesc.m_sCenter = D3DXVECTOR3(64,16,0);
	sliderDesc.backImage = "Slider_BackGround.png";
	sliderDesc.sPos = D3DXVECTOR3(751,155,0);
	sliderDesc.foreImage = "Slider_Slider.png";
	sliderDesc.maxX = 1;
	sliderDesc.minX = 0;
	sliderDesc.SliderVal = .5;
	sliderDesc.click = false;
	sliderDesc.sEffect = [&]()
	{

	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Slider("Slider3", &sliderDesc);
	mEditSliders.push_back((gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider3")));

	//============================================== Velocity ========================================================================
	//Size of the Particle
	tButtonDEsc.label = "Velocity X";
	tButtonDEsc.Posit = D3DXVECTOR3(700, 195,0);
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	tButtonDEsc.Center = D3DXVECTOR3(64, 16, 0);
	tButtonDEsc.tString = "Slider_BackGround.png";
	tButtonDEsc.click = false;
	tButtonDEsc.bEffect = [&]()
	{

	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("VelXButton", &tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("VelXButton"));

	//sets the arrow for the slider to increase it
	tButtonDEsc.label = "";
	tButtonDEsc.Posit = D3DXVECTOR3(783, 195, 0);
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	tButtonDEsc.Center = D3DXVECTOR3(16, 16,0);
	tButtonDEsc.tString = "Right_Arrow.png";
	tButtonDEsc.click = false;
	tButtonDEsc.bEffect = [&]()
	{
		if(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider4")->get_S_Pos() < 1)
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider4")->set_S_Val(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider4")->get_S_Pos() + .1);
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider4")->set_Slider_Pos(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider4")->get_Slider_Pos() + D3DXVECTOR3(+10,0,0));
		}
		else
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider4")->set_S_Val(1);
		}
	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("VelXUpButton", &tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("VelXUpButton"));

	//sets the left arrow for the slider to decrease it
	tButtonDEsc.label = "";
	tButtonDEsc.Posit = D3DXVECTOR3(615, 195, 0);
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	tButtonDEsc.Center = D3DXVECTOR3(16, 16,0);
	tButtonDEsc.tString = "Left_Arrow.png";
	tButtonDEsc.click = false;
	tButtonDEsc.bEffect = [&]()
	{
		if(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider4")->get_S_Pos() > 0)
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider4")->set_S_Val(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider4")->get_S_Pos() - .1);
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider4")->set_Slider_Pos(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider4")->get_Slider_Pos() + D3DXVECTOR3(-10,0,0));
		}
		else
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider4")->set_S_Val(0);
		}
	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("VelXDownButton", &tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("VelXDownButton"));

	//================================== Creates the Slider for Velocity X =================================================================
	//creates the slider.
	sliderDesc.m_sPosit = D3DXVECTOR3(700,195,1);
	sliderDesc.m_sVeloc = D3DXVECTOR3(0,0,0);
	sliderDesc.m_sCenter = D3DXVECTOR3(64,16,0);
	sliderDesc.backImage = "Slider_BackGround.png";
	sliderDesc.sPos = D3DXVECTOR3(751,195,0);
	sliderDesc.foreImage = "Slider_Slider.png";
	sliderDesc.maxX = 1;
	sliderDesc.minX = 0;
	sliderDesc.SliderVal = .5;
	sliderDesc.click = false;
	sliderDesc.sEffect = [&]()
	{

	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Slider("Slider4", &sliderDesc);
	mEditSliders.push_back((gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider4")));

	//Size of the Particle
	tButtonDEsc.label = "Velocity Y";
	tButtonDEsc.Posit = D3DXVECTOR3(700, 235,0);
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	tButtonDEsc.Center = D3DXVECTOR3(64, 16, 0);
	tButtonDEsc.tString = "Slider_BackGround.png";
	tButtonDEsc.click = false;
	tButtonDEsc.bEffect = [&]()
	{

	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("VelYButton", &tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("VelYButton"));

	//sets the arrow for the slider to increase it
	tButtonDEsc.label = "";
	tButtonDEsc.Posit = D3DXVECTOR3(783, 235, 0);
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	tButtonDEsc.Center = D3DXVECTOR3(16, 16,0);
	tButtonDEsc.tString = "Right_Arrow.png";
	tButtonDEsc.click = false;
	tButtonDEsc.bEffect = [&]()
	{
		if(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider5")->get_S_Pos() < 1)
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider5")->set_S_Val(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider5")->get_S_Pos() + .1);
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider5")->set_Slider_Pos(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider5")->get_Slider_Pos() + D3DXVECTOR3(+10,0,0));
		}
		else
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider5")->set_S_Val(1);
		}
	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("VelYUpButton", &tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("VelYUpButton"));

	//sets the left arrow for the slider to decrease it
	tButtonDEsc.label = "";
	tButtonDEsc.Posit = D3DXVECTOR3(615, 235, 0);
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	tButtonDEsc.Center = D3DXVECTOR3(16, 16,0);
	tButtonDEsc.tString = "Left_Arrow.png";
	tButtonDEsc.click = false;
	tButtonDEsc.bEffect = [&]()
	{
		if(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider5")->get_S_Pos() > 0)
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider5")->set_S_Val(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider5")->get_S_Pos() - .1);
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider5")->set_Slider_Pos(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider5")->get_Slider_Pos() + D3DXVECTOR3(-10,0,0));
		}
		else
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider5")->set_S_Val(0);
		}
	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("VelYDownButton", &tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("VelYDownButton"));

	//================================== Creates the Slider for Velocity Y =================================================================
	//creates the slider.
	sliderDesc.m_sPosit = D3DXVECTOR3(700,235,1);
	sliderDesc.m_sVeloc = D3DXVECTOR3(0,0,0);
	sliderDesc.m_sCenter = D3DXVECTOR3(64,16,0);
	sliderDesc.backImage = "Slider_BackGround.png";
	sliderDesc.sPos = D3DXVECTOR3(751,235,0);
	sliderDesc.foreImage = "Slider_Slider.png";
	sliderDesc.maxX = 1;
	sliderDesc.minX = 0;
	sliderDesc.SliderVal = .5;
	sliderDesc.click = false;
	sliderDesc.sEffect = [&]()
	{

	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Slider("Slider5", &sliderDesc);
	mEditSliders.push_back((gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider5")));

	//Size of the Particle
	tButtonDEsc.label = "Velocity Z";
	tButtonDEsc.Posit = D3DXVECTOR3(700, 275,0);
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	tButtonDEsc.Center = D3DXVECTOR3(64, 16, 0);
	tButtonDEsc.tString = "Slider_BackGround.png";
	tButtonDEsc.click = false;
	tButtonDEsc.bEffect = [&]()
	{

	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("VelZButton", &tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("VelZButton"));

	//sets the arrow for the slider to increase it
	tButtonDEsc.label = "";
	tButtonDEsc.Posit = D3DXVECTOR3(783, 275, 0);
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	tButtonDEsc.Center = D3DXVECTOR3(16, 16,0);
	tButtonDEsc.tString = "Right_Arrow.png";
	tButtonDEsc.click = false;
	tButtonDEsc.bEffect = [&]()
	{
		if(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider6")->get_S_Pos() < 1)
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider6")->set_S_Val(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider6")->get_S_Pos() + .1);
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider6")->set_Slider_Pos(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider6")->get_Slider_Pos() + D3DXVECTOR3(+10,0,0));
		}
		else
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider6")->set_S_Val(1);
		}
	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("VelZUpButton", &tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("VelZUpButton"));

	//sets the left arrow for the slider to decrease it
	tButtonDEsc.label = "";
	tButtonDEsc.Posit = D3DXVECTOR3(615, 275, 0);
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	tButtonDEsc.Center = D3DXVECTOR3(16, 16,0);
	tButtonDEsc.tString = "Left_Arrow.png";
	tButtonDEsc.click = false;
	tButtonDEsc.bEffect = [&]()
	{
		if(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider6")->get_S_Pos() > 0)
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider6")->set_S_Val(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider6")->get_S_Pos() - .1);
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider6")->set_Slider_Pos(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider6")->get_Slider_Pos() + D3DXVECTOR3(-10,0,0));
		}
		else
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider6")->set_S_Val(0);
		}
	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("VelZDownButton", &tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("VelZDownButton"));

	//================================== Creates the Slider for Velocity Z =================================================================
	//creates the slider.
	sliderDesc.m_sPosit = D3DXVECTOR3(700,275,1);
	sliderDesc.m_sVeloc = D3DXVECTOR3(0,0,0);
	sliderDesc.m_sCenter = D3DXVECTOR3(64,16,0);
	sliderDesc.backImage = "Slider_BackGround.png";
	sliderDesc.sPos = D3DXVECTOR3(751,275,0);
	sliderDesc.foreImage = "Slider_Slider.png";
	sliderDesc.maxX = 1;
	sliderDesc.minX = 0;
	sliderDesc.SliderVal = .5;
	sliderDesc.click = false;
	sliderDesc.sEffect = [&]()
	{

	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Slider("Slider6", &sliderDesc);
	mEditSliders.push_back((gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider6")));

	//============================================== Particles Per Sec(PPS) ========================================================================
	//Size of the Particle
	tButtonDEsc.label = "Particles Per Sec";
	tButtonDEsc.Posit = D3DXVECTOR3(700, 315,0);
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	tButtonDEsc.Center = D3DXVECTOR3(64, 16, 0);
	tButtonDEsc.tString = "Slider_BackGround.png";
	tButtonDEsc.click = false;
	tButtonDEsc.bEffect = [&]()
	{

	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("PPSButton", &tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("PPSButton"));

	//sets the arrow for the slider to increase it
	tButtonDEsc.label = "";
	tButtonDEsc.Posit = D3DXVECTOR3(783, 315, 0);
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	tButtonDEsc.Center = D3DXVECTOR3(16, 16,0);
	tButtonDEsc.tString = "Right_Arrow.png";
	tButtonDEsc.click = false;
	tButtonDEsc.bEffect = [&]()
	{
		if(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider7")->get_S_Pos() < 1)
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider7")->set_S_Val(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider7")->get_S_Pos() + .1);
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider7")->set_Slider_Pos(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider7")->get_Slider_Pos() + D3DXVECTOR3(11,0,0));
		}
		else
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider7")->set_S_Val(1);
		}
	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("PPSUpButton", &tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("PPSUpButton"));

	//sets the left arrow for the slider to decrease it
	tButtonDEsc.label = "";
	tButtonDEsc.Posit = D3DXVECTOR3(615, 315, 0);
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	tButtonDEsc.Center = D3DXVECTOR3(16, 16,0);
	tButtonDEsc.tString = "Left_Arrow.png";
	tButtonDEsc.click = false;
	tButtonDEsc.bEffect = [&]()
	{
		if(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider7")->get_S_Pos() > .1)
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider7")->set_S_Val(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider7")->get_S_Pos() - .1);
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider7")->set_Slider_Pos(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider7")->get_Slider_Pos() + D3DXVECTOR3(-11,0,0));
		}
		else
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider7")->set_S_Val(.1);
		}
	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("PPSDownButton", &tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("PPSDownButton"));

	//================================== Creates the Slider for Particles per Sec(PPS) =================================================================
	//creates the slider.
	sliderDesc.m_sPosit = D3DXVECTOR3(700,315,1);
	sliderDesc.m_sVeloc = D3DXVECTOR3(0,0,0);
	sliderDesc.m_sCenter = D3DXVECTOR3(64,16,0);
	sliderDesc.backImage = "Slider_BackGround.png";
	sliderDesc.sPos = D3DXVECTOR3(751,315,0);
	sliderDesc.foreImage = "Slider_Slider.png";
	sliderDesc.maxX = 1;
	sliderDesc.minX = 0;
	sliderDesc.SliderVal = .5;
	sliderDesc.click = false;
	sliderDesc.sEffect = [&]()
	{

	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Slider("Slider7", &sliderDesc);
	mEditSliders.push_back((gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider7")));

	//============================================== Number of Particles ========================================================================
	//Size of the Particle
	tButtonDEsc.label = "Number of Particles";
	tButtonDEsc.Posit = D3DXVECTOR3(700, 355,0);
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	tButtonDEsc.Center = D3DXVECTOR3(64, 16, 0);
	tButtonDEsc.tString = "Slider_BackGround.png";
	tButtonDEsc.click = false;
	tButtonDEsc.bEffect = [&]()
	{

	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("NumPButton", &tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("NumPButton"));

	//sets the arrow for the slider to increase it
	tButtonDEsc.label = "";
	tButtonDEsc.Posit = D3DXVECTOR3(783, 355, 0);
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	tButtonDEsc.Center = D3DXVECTOR3(16, 16,0);
	tButtonDEsc.tString = "Right_Arrow.png";
	tButtonDEsc.click = false;
	tButtonDEsc.bEffect = [&]()
	{
		if(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider8")->get_S_Pos() < 1)
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider8")->set_S_Val(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider8")->get_S_Pos() + .1);
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider8")->set_Slider_Pos(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider8")->get_Slider_Pos() + D3DXVECTOR3(11,0,0));
		}
		else
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider8")->set_S_Val(1);
		}
	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("NumPUpButton", &tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("NumPUpButton"));

	//sets the left arrow for the slider to decrease it
	tButtonDEsc.label = "";
	tButtonDEsc.Posit = D3DXVECTOR3(615, 355, 0);
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	tButtonDEsc.Center = D3DXVECTOR3(16, 16,0);
	tButtonDEsc.tString = "Left_Arrow.png";
	tButtonDEsc.click = false;
	tButtonDEsc.bEffect = [&]()
	{
		if(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider8")->get_S_Pos() > .1)
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider8")->set_S_Val(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider8")->get_S_Pos() - .1);
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider8")->set_Slider_Pos(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider8")->get_Slider_Pos() + D3DXVECTOR3(-11,0,0));
		}
		else
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider8")->set_S_Val(.1);
		}
	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("NumPDownButton", &tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("NumPDownButton"));

	//========================================== Number of Particles ==================================================================
	//creates the slider.
	sliderDesc.m_sPosit = D3DXVECTOR3(700,355,1);
	sliderDesc.m_sVeloc = D3DXVECTOR3(0,0,0);
	sliderDesc.m_sCenter = D3DXVECTOR3(64,16,0);
	sliderDesc.backImage = "Slider_BackGround.png";
	sliderDesc.sPos = D3DXVECTOR3(751,355,0);
	sliderDesc.foreImage = "Slider_Slider.png";
	sliderDesc.maxX = 1;
	sliderDesc.minX = 0;
	sliderDesc.SliderVal = .5;
	sliderDesc.click = false;
	sliderDesc.sEffect = [&]()
	{

	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Slider("Slider8", &sliderDesc);
	mEditSliders.push_back((gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider8")));

	//============================================== Creating the System ========================================================================
	////Size of the Particle
	//tButtonDEsc.label = "Build";
	//tButtonDEsc.Posit = D3DXVECTOR3(700, 450,0);
	//tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	//tButtonDEsc.Center = D3DXVECTOR3(64, 16, 0);
	//tButtonDEsc.tString = "Slider_BackGround.png";
	//tButtonDEsc.click = false;
	//tButtonDEsc.bEffect = [&]()
	//{
	//	D3DXVECTOR3 Veloc = D3DXVECTOR3(0,0,0);
	//	D3DXVECTOR3 tempWind = D3DXVECTOR3(0,-9.81,0);

	//	Veloc.x = gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider4")->get_S_Val() * 10;
	//	Veloc.y = gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider5")->get_S_Val() * 30;
	//	Veloc.z = gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider6")->get_S_Val() * 10;
	//	tempWind.x = gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider9")->get_S_Val() * 5;
	//	float tempTime = gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider7")->get_S_Val() / 10;
	//	float tempMass = gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider3")->get_S_Val() * 2;
	//	float tempSize = gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider2")->get_S_Val() * 50;
	//	float tempLife = gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider1")->get_S_Val() * 5; 
	//	float tempNum = gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider8")->get_S_Val() * 10000; 

	//	gd3dApp->get_R_Mgr()->get_ParticleMgr()->Add_Particle("Sprinkler", Veloc, tempWind, tempNum, tempTime, tempLife, tempSize, tempMass);
	//	mGfx_Stats->addTriangles(tempNum);

	//};
	//gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("BuildButton", &tButtonDEsc);
	//mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("BuildButton"));

	//==============================================  Resetting the System ========================================================================
	//Size of the Particle
	tButtonDEsc.label = "Reset";
	tButtonDEsc.Posit = D3DXVECTOR3(550, 550,0);
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	tButtonDEsc.Center = D3DXVECTOR3(64, 16, 0);
	tButtonDEsc.tString = "Slider_BackGround.png";
	tButtonDEsc.click = false;
	tButtonDEsc.bEffect = [&]()
	{
		gd3dApp->get_R_Mgr()->get_ParticleMgr()->ResetVector();
		mGfx_Stats->setTriCount(0);
	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("ResetButton", &tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("ResetButton"));

	//============================================== Wind Direction ========================================================================
	//Size of the Particle
	tButtonDEsc.label = "<- WindX ->";
	tButtonDEsc.Posit = D3DXVECTOR3(700, 395,0);
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	tButtonDEsc.Center = D3DXVECTOR3(64, 16, 0);
	tButtonDEsc.tString = "Slider_BackGround.png";
	tButtonDEsc.click = false;
	tButtonDEsc.bEffect = [&]()
	{

	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("WindButton", &tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("WindButton"));

	//sets the arrow for the slider to increase it
	tButtonDEsc.label = "";
	tButtonDEsc.Posit = D3DXVECTOR3(783, 395, 0);
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	tButtonDEsc.Center = D3DXVECTOR3(16, 16,0);
	tButtonDEsc.tString = "Right_Arrow.png";
	tButtonDEsc.click = false;
	tButtonDEsc.bEffect = [&]()
	{
		if(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider9")->get_S_Pos() < .5)
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider9")->set_S_Val(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider9")->get_S_Pos() + .1);
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider9")->set_Slider_Pos(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider9")->get_Slider_Pos() + D3DXVECTOR3(11,0,0));
		}
		else 
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider9")->set_S_Val(.5);
		}
	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("WindRightButton", &tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("WindRightButton"));

	//sets the left arrow for the slider to decrease it
	tButtonDEsc.label = "";
	tButtonDEsc.Posit = D3DXVECTOR3(615, 395, 0);
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	tButtonDEsc.Center = D3DXVECTOR3(16, 16,0);
	tButtonDEsc.tString = "Left_Arrow.png";
	tButtonDEsc.click = false;
	tButtonDEsc.bEffect = [&]()
	{
		if(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider9")->get_S_Pos() > -.5)
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider9")->set_S_Val(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider9")->get_S_Pos() - .1);
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider9")->set_Slider_Pos(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider9")->get_Slider_Pos() + D3DXVECTOR3(-11,0,0));
		}
		else 
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider9")->set_S_Val(-.5);
		}
	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("WindLeftButton", &tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("WindLeftButton"));

	//========================================== Wind Slider ===================================================================
	//creates the slider.
	sliderDesc.m_sPosit = D3DXVECTOR3(700,395,1);
	sliderDesc.m_sVeloc = D3DXVECTOR3(0,0,0);
	sliderDesc.m_sCenter = D3DXVECTOR3(64,16,0);
	sliderDesc.backImage = "Slider_BackGround.png";
	sliderDesc.sPos = D3DXVECTOR3(751,395,0);
	sliderDesc.foreImage = "Slider_Slider.png";
	sliderDesc.maxX = 1;
	sliderDesc.minX = 0;
	sliderDesc.SliderVal = 0;
	sliderDesc.click = false;
	sliderDesc.sEffect = [&]()
	{

	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Slider("Slider9", &sliderDesc);
	mEditSliders.push_back((gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider9")));

	//============================================== Color Direction ========================================================================
	//Size of the Particle
	tButtonDEsc.label = "COLOR";
	tButtonDEsc.Posit = D3DXVECTOR3(700, 435,0);
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	tButtonDEsc.Center = D3DXVECTOR3(64, 16, 0);
	tButtonDEsc.tString = "Slider_BackGround.png";
	tButtonDEsc.click = false;
	tButtonDEsc.bEffect = [&]()
	{

	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("ColorButton", &tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("ColorButton"));

	//sets the arrow for the slider to increase it
	tButtonDEsc.label = "";
	tButtonDEsc.Posit = D3DXVECTOR3(783, 435, 0);
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	tButtonDEsc.Center = D3DXVECTOR3(16, 16,0);
	tButtonDEsc.tString = "Right_Arrow.png";
	tButtonDEsc.click = false;
	tButtonDEsc.bEffect = [&]()
	{
		if(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider10")->get_S_Pos() < 10)
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider10")->set_S_Val(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider10")->get_S_Pos() + 1);
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider10")->set_Slider_Pos(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider10")->get_Slider_Pos() + D3DXVECTOR3(11,0,0));
		}
		else 
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider10")->set_S_Val(10);
		}
	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("ColorRightButton", &tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("ColorRightButton"));

	//sets the left arrow for the slider to decrease it
	tButtonDEsc.label = "";
	tButtonDEsc.Posit = D3DXVECTOR3(615, 435, 0);
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);
	tButtonDEsc.Center = D3DXVECTOR3(16, 16,0);
	tButtonDEsc.tString = "Left_Arrow.png";
	tButtonDEsc.click = false;
	tButtonDEsc.bEffect = [&]()
	{
		if(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider10")->get_S_Pos() > 1)
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider10")->set_S_Val(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider10")->get_S_Pos() - 1);
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider10")->set_Slider_Pos(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider10")->get_Slider_Pos() + D3DXVECTOR3(-11,0,0));
		}
		else 
		{
			gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider10")->set_S_Val(1);
		}
	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("ColorLeftButton", &tButtonDEsc);
	mEditButtons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("ColorLeftButton"));

	//========================================== Color Slider =================================================================
	//creates the slider.
	sliderDesc.m_sPosit = D3DXVECTOR3(700,435,1);
	sliderDesc.m_sVeloc = D3DXVECTOR3(0,0,0);
	sliderDesc.m_sCenter = D3DXVECTOR3(64,16,0);
	sliderDesc.backImage = "Slider_BackGround.png";
	sliderDesc.sPos = D3DXVECTOR3(751,435,0);
	sliderDesc.foreImage = "Slider_Slider.png";
	sliderDesc.maxX = 1;
	sliderDesc.minX = 0;
	sliderDesc.SliderVal = 5;
	sliderDesc.click = false;
	sliderDesc.sEffect = [&]()
	{

	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Slider("Slider10", &sliderDesc);
	mEditSliders.push_back((gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider10")));
}

ParticleShow::ParticleShow(FSM *fsm)
{
	stateName = "ParticleShow";
	myFsm = fsm;
}

ParticleShow::~ParticleShow(void)
{

}

void ParticleShow::onResetDevice()
{

}

void ParticleShow::onLostDevice()
{

}

void ParticleShow::UpdateScene(float dt)
{
	mGfx_Stats->update(dt);

	for(auto it : mEditButtons)
	{
		it->updateScene(dt);
	}

	for(auto it : mEditSliders)
	{
		it->updateScene(dt);
	}

	m_Clicked = false;
	gDInput->poll();

	if(!m_Clicked)
	{
		POINT mouse;
		GetCursorPos(&mouse);
		ScreenToClient(gd3dApp->getMainWnd(), &mouse);
		D3DXVECTOR3 Position = D3DXVECTOR3((mouse.x - 400) / 6, (mouse.y -300) / -6, 20);

		if(gDInput->keyDown(DIK_I))
		{
			if(gd3dApp->LMB_is_Clicked() == true)
			{
				D3DXVECTOR3 Veloc = D3DXVECTOR3(0,0,0);
				D3DXVECTOR3 tempWind = D3DXVECTOR3(0,-9.81,0);

				Veloc.x = gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider4")->get_S_Val() * 10;
				Veloc.y = gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider5")->get_S_Val() * 30;
				Veloc.z = gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider6")->get_S_Val() * 10;
				tempWind.x = gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider9")->get_S_Val() * 10;
				float tempTime = gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider7")->get_S_Val() / 10;
				float tempMass = gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider3")->get_S_Val() * 2;
				float tempSize = gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider2")->get_S_Val() * 50;
				float tempLife = gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider1")->get_S_Val() * 5; 
				float tempNum = gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider8")->get_S_Val() * 10000;
				int tempColor = gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Slider("Slider10")->get_S_Val();

				gd3dApp->get_R_Mgr()->get_ParticleMgr()->Add_Particle("Sprinkler", Veloc, tempWind, Position, tempNum, tempTime, tempLife, tempSize, tempMass, tempColor);
				mGfx_Stats->addTriangles(tempNum);
			}
		}		
	}
	gd3dApp->get_R_Mgr()->get_ParticleMgr()->UpDate(dt);
}

void ParticleShow::RenderScene()
{
	mGfx_Stats->display();

	gd3dApp->get_R_Mgr()->get_Sprite()->Begin(16);
	//iterate over buttons calling render
	for(auto it : mEditButtons)
	{
		it->render(gd3dApp->get_R_Mgr()->get_Sprite());
	}
	gd3dApp->get_R_Mgr()->get_Sprite()->Flush();

	for(auto ij : mEditSliders)
	{
		ij->render(gd3dApp->get_R_Mgr()->get_Sprite());
	}
	gd3dApp->get_R_Mgr()->get_Sprite()->Flush();

	gd3dApp->get_R_Mgr()->get_ParticleMgr()->Draw();
}

void ParticleShow::InitializeState()
{
	for(auto butns : mEditButtons)
	{
		butns->resetButton();
	}
}

void ParticleShow::LeaveState()
{

}