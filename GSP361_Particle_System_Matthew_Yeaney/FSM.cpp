//FSM.cpp
#include "FSM.h"

//Constructor
FSM::FSM()
{
	currentState = NULL;
}

//Destructor
FSM::~FSM()
{
	stateBank.clear();
}

//Update each tick
void FSM::Update(const float& dt)
{
	//Make sure a current state is loaded
	if (currentState == NULL) return;

	static bool bStart = true;

	if(bStart == true)
	{
		currentState->InitializeState();
		bStart = false;
	}

	//Update the current state, may trigger a transition.
	currentState->UpdateScene(dt);
}

//Called to transition to another state
//@param stateName the name of the state to transition to
void FSM::TransitionTo(std::string stateName)
{
	//Find the named state
	FSMState *goToState = NULL;
	for(iter= stateBank.begin(); iter!= stateBank.end(); iter++)
	if ( (*iter)->stateName == stateName )
		goToState = iter->get();

	//Error, trying to transition to a non-existant state
	if (goToState == NULL)
	{
	//Print an error here, or assert if you want
	return;
	}

	currentState->LeaveState();
	goToState->InitializeState();
	currentState = goToState;
}

//Add a state to the bank, optionally make it the current state
//@param newState the new state to add to the state machine
//@param makeCurrent is this new state the current state?
void FSM::AddState(FSMState *newState, bool makeCurrent)
{
	//Add this state to the FSM
	std::tr1::shared_ptr<FSMState> newStatePtr(newState);
	stateBank.push_back(newStatePtr);
	//Make this the current state?
	if (makeCurrent) currentState = newState;
}

//What is the name of the current state?
std::string FSM::GetState()
{
	return currentState->stateName;
}