#include "Sprite_Base.h"


Sprite_Base::Sprite_Base(void)
{
	//Position
	mPosition.x = 0;
	mPosition.y = 0;
	mPosition.z = 0;
	
	//center
	mCenter.x = 0;
	mCenter.y = 0;
	mCenter.z = 0;
	
	//Velocity
	mVelocity.x = 0;
	mVelocity.y = 0;
	mVelocity.z = 0;
}

Sprite_Base::Sprite_Base(D3DXVECTOR3 Pos, D3DXVECTOR3 Vel, D3DXVECTOR3 Cen)
{
	mPosition = Pos;
	mCenter = Cen;
	mVelocity = Vel;	

}

Sprite_Base::~Sprite_Base(void)
{

}


