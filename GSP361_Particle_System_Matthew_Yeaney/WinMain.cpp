//this is the Windows main that will be used to call EVERYTHING
#include "CodeMain.h"
#include "d3dApp.h"
#include <crtdbg.h>
#include "Gfx_Stats.h"
#include "DirectInput.h"
#include "Resource_Manager.h"
#include "State_Manager.h"
#include "Menu_State.h"
#include <vld.h>


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
				   PSTR cmdLine, int showCmd)
{
	// Enable run-time memory check for debug builds.
	#if defined(DEBUG) | defined(_DEBUG)
		_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
	#endif

	CodeMain app(hInstance, "Matthew_Yeaney", D3DDEVTYPE_HAL, D3DCREATE_HARDWARE_VERTEXPROCESSING);
	gd3dApp = &app;
	gd3dApp->Initialize();
	// instantiates the Input inside the WinMain for Keyboard and mouse
	DirectInput di(DISCL_NONEXCLUSIVE | DISCL_FOREGROUND,
				   DISCL_NONEXCLUSIVE | DISCL_FOREGROUND);
	gDInput = &di;

	return gd3dApp->run();
}

CodeMain::CodeMain(HINSTANCE hInstance, std::string winCaption, D3DDEVTYPE devType, DWORD requestedVP)
: D3DApp(hInstance, winCaption, devType, requestedVP)
{
	if(!checkDeviceCaps())
	{
		MessageBox(0, "checkDeviceCaps() Failed", 0, 0);
		PostQuitMessage(0);
	}
}

CodeMain::~CodeMain()
{
	delete mGfx_Stats;
	delete R_Manager;
	delete S_Manager;
}

bool CodeMain::checkDeviceCaps()
{
	// Nothing to check.
	return true;
}

void CodeMain::Initialize()
{
	InitVertexDeclarations(gd3dDevice);

	//initialization of the resource manager
	R_Manager = new Resource_Manager(gd3dDevice);
	S_Manager = new State_Manager();	
	//creates the Graphics count and frame rates monitor
	mGfx_Stats = new Gfx_Stats();

	S_Manager->switchState("mainMenu");

}

void CodeMain::ResetDevice()
{

	// will reset the state, if lost
	R_Manager->ResetResources();
	S_Manager->m_pMyFSM->currentState->onLostDevice();
	S_Manager->m_pMyFSM->currentState->onResetDevice();
	

	/*Particles->onResetDevice();
	Particles->onLostDevice();*/
}

void CodeMain::updateScene(float dt)
{

	S_Manager->m_pMyFSM->currentState->UpdateScene(dt);
	if(S_Manager->m_pMyFSM->currentState->stateName == "mainMenu")
	mGfx_Stats->update(dt);
	
}

void CodeMain::drawScene()
{
	HR(gd3dDevice->Clear(0, NULL, /*flags - clear the target surface*/D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0,0,0), /*Z*/ 1.0f, /*0 for stencil*/0));
	
	HR(gd3dDevice->BeginScene());


	//should get the Particle System that is being called and draw it
	//gd3dApp->get_R_Mgr()->get_ParticleMgr()->Draw();
	//Particles->draw();
	if(S_Manager->m_pMyFSM->currentState->stateName == "mainMenu")
	mGfx_Stats->display();
	S_Manager->m_pMyFSM->currentState->RenderScene();

	HR(gd3dDevice->EndScene());
	HR(gd3dDevice->Present(0, 0, 0, 0));
}

Resource_Manager* CodeMain::get_R_Mgr()
{
	return R_Manager;
}

State_Manager* CodeMain::StateManaged()
{
	return S_Manager;
}

