#pragma once
#include "d3dApp.h"
#include <crtdbg.h>
#include <vector>
#include "Vertex.h"

class Gfx_Stats;
class Resource_Manager;
class State_Manager;

class CodeMain : public D3DApp
{
public:
	CodeMain(HINSTANCE hInstance, std::string winCaption, D3DDEVTYPE devType, DWORD requestedVP);
	~CodeMain();

	bool checkDeviceCaps();
	//void onLostDevice();
	//void onResetDevice();
	void ResetDevice();
	void updateScene(float dt);
	void drawScene();

	Resource_Manager* get_R_Mgr();

	void Initialize()override;	
	State_Manager* StateManaged()override;


	

private:
	Gfx_Stats* mGfx_Stats;
	Resource_Manager* R_Manager;
	State_Manager* S_Manager;
	
	
};

