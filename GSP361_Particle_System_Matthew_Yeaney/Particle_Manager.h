#pragma once

#include <map>
#include <vector>
#include <string>
#include <d3d9.h>
#include <d3dx9.h>
#include <DxErr.h>
#include "d3dApp.h"
#include "GameCamera.h"
#include "Vertex.h"

class PSystem;
class Sprite_Button;
//class Particle;

class Particle_Manager
{
private:
	std::vector<PSystem*> m_Vector_Of_Sprinkler_Systems;
	std::map<std::string, PSystem*> m_Map_Of_Particle_Systems;

public:

	Particle_Manager(void);
	~Particle_Manager(void);

	void Add_Particle(std::string, D3DXVECTOR3, D3DXVECTOR3, D3DXVECTOR3, float, float, float, float, float, int);
	PSystem* Get_System(std::string name);

	void Draw();
	void UpDate(float dt);

	void onLostDevice();
	void onResetDevice();
	void ResetVector();
	
};
