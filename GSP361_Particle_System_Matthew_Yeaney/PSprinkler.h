#pragma once

#include <d3d9.h>
#include <d3dx9.h>
#include <DxErr.h>
#include "PSystem.h"
#include "Particle_Manager.h"
#include "d3dUtil.h"


class PSprinkler : public PSystem
{
public:
	PSprinkler(const std::string& fxName,
		const std::string& techName,
		const std::string& texName, 
		const D3DXVECTOR3& accel,
		D3DXVECTOR3 Pos, 
		int maxNumParticles,
		float timePerSecond)
		: PSystem(fxName, techName, texName, accel, maxNumParticles,
		timePerSecond)
	{
		ePosition = Pos;
	}
private:	
	//for Editing System
	D3DXVECTOR3 ePosition;
	D3DXVECTOR3 eVelocity;			//editable Velocity
	float       eSize;				//editable Size
	float       eTime;				//editable time
	float       eLifeTime;			//editable Lifetime
	float       eMass;				//editable mass
	D3DCOLOR    eColor;				//editable Color

public:
	//accessors for editing
	//Velocity
	D3DXVECTOR3 getVelocity(){return eVelocity;}
	float getVelocityX(){ return eVelocity.x;}
	float getVelocityY(){ return eVelocity.y;}
	float getVelocityZ(){ return eVelocity.z;}
	//Size
	float get_e_Size(){return eSize;}
	//time
	float get_e_Lifetime(){return eLifeTime;}
	float get_e_Time(){return eTime;}
	//Mass
	float get_e_Mass(){return eMass;}
	//Color
	D3DCOLOR get_e_Color(){return eColor;}

	//mutators for editing
	//Velocities
	D3DXVECTOR3 SetVeloc(D3DXVECTOR3 Veloc){ eVelocity = Veloc;}
	void SetXVeloc(float VelX){ eVelocity.x = VelX;}
	void setYVeloc(float VelY){ eVelocity.y = VelY;}
	void setZVeloc(float VelZ){ eVelocity.z = VelZ;}
	//Size
	void Set_e_Size(float S){ eSize = S;}
	//Time
	void Set_e_Time(float t){ eTime = t;}
	void set_e_LifeTime(float lt){ eLifeTime = lt;}
	//Mass
	void set_e_Mass(float m){ eMass = m;}
	//color
	void set_e_Color(D3DCOLOR c){ eColor = c;}
	
	void initParticle(Particle& out)
	{
		// Generate about the origin.
		out.initialPos = ePosition; /*D3DXVECTOR3(0.0f, 0.0f, 0.0f);*/
 
		out.initialTime     = mTime;
		out.lifeTime        = GetRandomFloat(1.0f, get_e_Lifetime());
		out.initialColor    = get_e_Color();
		out.initialSize     = get_e_Size();
		out.mass            = get_e_Mass();

		out.initialVelocity.x = GetRandomFloat(-2.5f, getVelocityX());
		out.initialVelocity.y = GetRandomFloat(5.0f , getVelocityY());
		out.initialVelocity.z = GetRandomFloat(-2.5f, getVelocityZ());
	}

	

};
