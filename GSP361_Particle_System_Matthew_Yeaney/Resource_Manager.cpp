#include "Resource_Manager.h"
#include "Font_Manager.h"
#include "Particle_Manager.h"
#include "Button_Manager.h"
#include "Texture_Manager.h"

#include "d3dApp.h"



Resource_Manager::Resource_Manager(void)
{
	g3DDevice = nullptr;
	mFontManager = nullptr;
	mFontManager = new Font_Manager();
	mTextureMgr = nullptr;
	mTextureMgr = new Texture_Manager();
	mButtonMgr = nullptr;
	mButtonMgr = new Button_Manager();
	mParticleMgr = nullptr;
	mParticleMgr = new Particle_Manager();
}

Resource_Manager::Resource_Manager(IDirect3DDevice9* gDevice)
{
	g3DDevice = gDevice;
	mFontManager = nullptr;
	mFontManager = new Font_Manager();
	mTextureMgr = new Texture_Manager(gDevice);
	D3DXCreateSprite(g3DDevice, &m_Sprite);
	mButtonMgr = nullptr;
	mButtonMgr = new Button_Manager();
	mParticleMgr = nullptr;
	mParticleMgr = new Particle_Manager();
}

Resource_Manager::~Resource_Manager(void)
{
	m_Sprite->Release();
	if(mFontManager!=nullptr)
	{
		delete mFontManager;
	}
	if(mParticleMgr != nullptr)
	{
		delete mParticleMgr;
	}
}

Font_Manager* Resource_Manager::get_FontMgr()
{
	return mFontManager;
}

Texture_Manager* Resource_Manager::get_TexMgr()
{
	return mTextureMgr;
}

Button_Manager* Resource_Manager::get_ButtMgr()
{
	return mButtonMgr;
}

Particle_Manager* Resource_Manager::get_ParticleMgr()
{
	return mParticleMgr;
}

ID3DXSprite* Resource_Manager::get_Sprite()
{
	return m_Sprite;
}

void Resource_Manager::ResetResources()
{
	mFontManager->onLostDevice();
	mFontManager->onResetDevice();
	mParticleMgr->onLostDevice();
	mParticleMgr->onResetDevice();
}
