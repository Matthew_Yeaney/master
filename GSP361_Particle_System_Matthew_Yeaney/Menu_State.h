#pragma once
#include "FSM.h"
#include <vector>
#include <d3d9.h>
#include <d3dx9.h>
#include <DxErr.h>

class Resource_Manager;
class Sprite_Button;

class Menu_State :	public FSMState
{
private:
	// add storage for buttons
	std::vector<Sprite_Button*> m_Buttons;

public:

	Menu_State(void);
	Menu_State(FSM *fsm);
	~Menu_State(void);
	
	void onResetDevice();
	void onLostDevice();

	void UpdateScene(float dt);
	void RenderScene();

	void InitializeState();
	void LeaveState();

};

