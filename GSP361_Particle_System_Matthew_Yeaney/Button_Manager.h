#pragma once
#include <map>
#include <vector>
#include <string>
#include <d3d9.h>
#include <d3dx9.h>
#include <DxErr.h>

class Sprite_Button;
class Sprite_Slider;
struct Button_Desc;
struct Slider_Desc;

class Button_Manager
{
private:
	std::vector<Sprite_Button*> m_VectorOfButtons;
	std::map<std::string, Sprite_Button*> m_MapOfButtons;
	std::vector<Sprite_Slider*> m_VectorOfSliders;
	std::map<std::string, Sprite_Slider*> m_MapOfSliders;

public:
	Button_Manager(void);
	~Button_Manager(void);

	//registers the buttons into the map and vector
	bool Register_Button(std::string name, Button_Desc* buttonDescStruct);
	bool Register_Slider(std::string name, Slider_Desc* SliderDescStruct);

	//this will return the button
	Sprite_Button* get_Button(std::string name);
	Sprite_Slider* get_Slider(std::string name);

};

