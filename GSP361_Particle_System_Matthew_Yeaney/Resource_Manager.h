#pragma once
#include <map>
#include <vector>
#include <d3d9.h>
#include <d3dx9.h>
#include <DxErr.h>

class Font_Manager;
class Texture_Manager;
class Button_Manager;
class Particle_Manager;


class Resource_Manager
{
private:
	Font_Manager* mFontManager;
	Texture_Manager* mTextureMgr;
	Button_Manager* mButtonMgr;
	Particle_Manager* mParticleMgr;

	ID3DXSprite* m_Sprite;
	IDirect3DDevice9* g3DDevice;
	


public:
	Resource_Manager(void);
	Resource_Manager(IDirect3DDevice9* /*gDevice*/);
	~Resource_Manager(void);

	//accessors
	Font_Manager* get_FontMgr();
	Texture_Manager* get_TexMgr();
	Button_Manager* get_ButtMgr();
	Particle_Manager* get_ParticleMgr();


	ID3DXSprite* get_Sprite();

	void ResetResources();
 
};