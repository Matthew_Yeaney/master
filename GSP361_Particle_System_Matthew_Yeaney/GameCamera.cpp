#include "GameCamera.h"
#include "DirectInput.h"

GameCamera::GameCamera(void)
{
	D3DXMatrixIdentity(&mWorld);
	D3DXMatrixIdentity(&mProject);

	Up = D3DXVECTOR3(0.0f,1.0f,0.0f);
	Right = D3DXVECTOR3(1.0f,0.0f,0.0f);
	Forward = D3DXVECTOR3(0.0f,0.0f,1.0f);

	Speed = 10.0f;
	Sensitivity = 1.0f;
	Position = D3DXVECTOR3(0,0,0);
}

GameCamera::~GameCamera(void)
{

}

bool GameCamera::Initialize(D3DXVECTOR3 position, D3DXVECTOR3 target, HWND hWnd)
{
	Position = position;
	D3DXMatrixTranslation(&mWorld, Position.x, Position.y, Position.z);
	Target = target;

	BuildViewMatrix();
	BuildProjMatrix(hWnd);

	return true;
}

void GameCamera::Update(float dt, DirectInput* dInput)
{
	BuildViewMatrix();
	Rotate(dt, dInput);
	Adjust(dt, dInput);
}

void GameCamera::BuildViewMatrix()
{
	D3DXVec3Normalize(&Forward, &Forward);
	//Recalculate the Up vector
	D3DXVec3Cross(&Up, &Forward, &Right);
	D3DXVec3Normalize(&Up, &Up);

	D3DXVec3Normalize(&Right, &Right);

	float x = -D3DXVec3Dot(&Position, &Right);
	float y = -D3DXVec3Dot(&Position, &Up);
	float z = -D3DXVec3Dot(&Position, &Forward);

	mWorld(0,0) = Right.x; 
	mWorld(1,0) = Right.y; 
	mWorld(2,0) = Right.z; 
	mWorld(3,0) = x;   

	mWorld(0,1) = Up.x;
	mWorld(1,1) = Up.y;
	mWorld(2,1) = Up.z;
	mWorld(3,1) = y;  

	mWorld(0,2) = Forward.x; 
	mWorld(1,2) = Forward.y; 
	mWorld(2,2) = Forward.z; 
	mWorld(3,2) = z;   

	mWorld(0,3) = 0.0f;
	mWorld(1,3) = 0.0f;
	mWorld(2,3) = 0.0f;
	mWorld(3,3) = 1.0f;
}

void GameCamera::BuildProjMatrix(HWND hWnd)
{
	RECT temp;
	GetClientRect(hWnd, &temp);
	float w = (float)temp.right;
	float h = (float)temp.bottom;
	D3DXMatrixPerspectiveFovLH(&mProject, D3DX_PI * 0.25f, w/h, 0.1f, 5000.0f);
}

void GameCamera::Rotate(float dt, DirectInput* dInput)
{
	//If right mouse button are down
	if(dInput->mouseButtonDown(1)) 
	{ 

		float pitch = dInput->mouseDY() * dt;
		float yAngle = dInput->mouseDX() * dt;

		pitch *= Sensitivity;
		yAngle *= Sensitivity;

		D3DXMATRIX mRotAxis;
		D3DXMatrixRotationAxis(&mRotAxis, &Right, pitch);
		D3DXVec3TransformCoord(&Forward, &Forward, &mRotAxis);
		D3DXVec3TransformCoord(&Up, &Up, &mRotAxis);

		D3DXMATRIX mRotYAxis;
		D3DXMatrixRotationY(&mRotYAxis, yAngle);
		D3DXVec3TransformCoord(&Right, &Right, &mRotYAxis);
		D3DXVec3TransformCoord(&Up, &Up, &mRotYAxis);
		D3DXVec3TransformCoord(&Forward, &Forward, &mRotYAxis);

	}
}

void GameCamera::Adjust(float dt, DirectInput* dInput)
{
	float speedSet = Speed;
	if(dInput->keyDown(DIK_LSHIFT)) 
	{
		speedSet *= 5;
	}

	D3DXVECTOR3 direction(0.0f, 0.0f, 0.0f);

	if(dInput->keyDown(DIK_W))
	{
		direction += Forward;
	}

	if(dInput->keyDown(DIK_S))
	{ 
		direction -= Forward;
	}

	if(dInput->keyDown(DIK_D))
	{
		direction += Right;
	}

	if(dInput->keyDown(DIK_A))
	{ 
		direction -= Right;
	}

	if(dInput->keyDown(DIK_Q))
	{
		direction.y += 1;
	}

	if(dInput->keyDown(DIK_E))
	{
		direction.y -= 1;
	}

	D3DXVec3Normalize(&direction, &direction);
	Position += direction * speedSet * dt;
}

void GameCamera::SetPosition(D3DXVECTOR3 pos)
{
	Position = pos;
}

void GameCamera::SetTarget(D3DXVECTOR3 target)
{
	Target = target;
}
