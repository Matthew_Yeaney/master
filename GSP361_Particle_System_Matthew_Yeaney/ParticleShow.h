#pragma once
#include "FSM.h"
#include <vector>
#include <d3d9.h>
#include <d3dx9.h>
#include <DxErr.h>
#include "Gfx_Stats.h"

class Sprite_Button;
class Sprite_Slider;

class ParticleShow : public FSMState
{
private:
	std::vector<Sprite_Button*> mEditButtons;
	std::vector<Sprite_Slider*> mEditSliders;

public:
	ParticleShow(void);
	ParticleShow(FSM *fsm);
	~ParticleShow(void);

	//variables
	D3DXVECTOR3 Veloc, tempWind;
	float tempTime, tempMass, tempSize, tempLife, tempNum;
	float TotalParticles, TotalAlive, TotalDead;
	int tempColor;
	bool m_Clicked;
	

	Gfx_Stats* mGfx_Stats;

	//methods
	void onResetDevice();
	void onLostDevice();

	void UpdateScene(float dt);
	void RenderScene();

	void InitializeState();
	void LeaveState();

	void resetButton();

};

