#include "Sprite_Slider.h"
#include "d3dApp.h"
#include "Texture_Manager.h"
#include "Resource_Manager.h"
#include "Font_Manager.h"

Sprite_Slider::Sprite_Slider(void)
{

}

Sprite_Slider::Sprite_Slider(D3DXVECTOR3 Pos, D3DXVECTOR3 Vel, D3DXVECTOR3 Cen,
							 D3DXVECTOR3 sPosition,
							 std::function<void(void)> sEffect,
							 std::string tex_name1,
							 std::string tex_name2,
							 double min_X,
							 double max_X,
							 double sliderVal,
							 bool clickable) : Sprite_Base(Pos, Vel, Cen)
{
	//access to the resource manager to get a texture
	mBackTexture = gd3dApp->get_R_Mgr()->get_TexMgr()->get_Texture(tex_name1); 
	mForeTexture = gd3dApp->get_R_Mgr()->get_TexMgr()->get_Texture(tex_name2);
	mSliderEffect = sEffect;
	m_Slider_Pos = sPosition;			//for the slider to move back and forth on, or raise up and down
	m_Clicked = clickable;				//for a button adjustment slider
	s_Val = sliderVal;
}

Sprite_Slider::~Sprite_Slider(void)
{

}


void Sprite_Slider::updateScene(float dt)
{
	this->set_Pos(this->getPos() + this->getVel());
}

void Sprite_Slider::render(ID3DXSprite *Sprite)
{
	//creates a matrix 4x4 for M and T.
	D3DXMATRIX M,T;
	//sets M to the Identity Matrix
	D3DXMatrixIdentity(&M);
	//sets T to the Translation Matrix 4x4;
	D3DXMatrixTranslation(&T, this->get_X_Pos(), this->get_Y_Pos(),0);
	//this will set the TransformMatrix to the Identity Matrix
	Sprite->SetTransform(&M);
	Sprite->Draw(mBackTexture, 0, &this->getCen(), &this->getPos(), D3DCOLOR_RGBA(0,255,0,255)/*0xFF008000*/);
	Sprite->Draw(mForeTexture, 0, &this->getCen(), &this->get_Slider_Pos(), D3DCOLOR_RGBA(255,0,0,255)/*0xFF008000*/);
	//sets the transform Matrix to the T matrix which has been translated
	Sprite->SetTransform(&T);
	//create the RECT
	RECT rect = {0,0,0,0};
	//Create the text on the Button

	gd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
}

bool Sprite_Slider::OverButton(float X_mouse, float Y_mouse)
{
	if(this->get_X_Pos() - this->get_X_Cen() > X_mouse)
	{
		return false;
	}
	if(this->get_X_Pos() + this->get_X_Cen() < X_mouse)
	{
		return false;
	}
	if(this->get_Y_Pos() - this->get_Y_Cen() > Y_mouse)
	{
		return false;
	}
	if(this->get_Y_Pos() + this->get_Y_Cen() < Y_mouse)
	{
		return false;
	}
	else
	{
		return true;
	}
}

void Sprite_Slider::onClick()
{
	if(!m_Clicked)
	{
		POINT mouse;
		GetCursorPos(&mouse);
		ScreenToClient(gd3dApp->getMainWnd(), &mouse);
		if(OverButton((float)mouse.x, (float)mouse.y))
		{
			if(gd3dApp->LMB_is_Clicked() == true)
			{
				this->mSliderEffect();			
			}
		}
	}
}

void Sprite_Slider::resetButton()
{
	if(m_Clicked == true)
	{
		m_Clicked = false;
	}
}


void Sprite_Slider::setUpVal()// this determines where to move the slider based on the value
{
	s_Val != -.1 && s_Val != 1.1;
	double left = this->get_X_Pos() + s_Val * 0.90f;
	double right = this->get_X_Pos() + s_Val * 0.90f;
	double offSet = right - left;
	m_Slider_Pos.x = left + offSet * s_Val;
}
