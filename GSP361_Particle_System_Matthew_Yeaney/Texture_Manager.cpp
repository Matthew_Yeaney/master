#include "Texture_Manager.h"



Texture_Manager::Texture_Manager(void)
{

}

Texture_Manager::~Texture_Manager(void)
{
	//release all the Textures
	for(auto it : m_VectorOfTextures)
	{
		it->Release();
	}
	/*for(auto ij : m_MapOfTextures)
	{
		ij.second->Release();
	}*/
}

Texture_Manager::Texture_Manager(IDirect3DDevice9* gDDevice)
{
	//passes in the global device;
	gDevice = gDDevice;
}

bool Texture_Manager::Register_Texture(std::string name)
{
	//finds out if the texture is already in the map
	auto retVal = m_MapOfTextures.find(name);
	//if not, then it implements it into the map
	if(retVal != m_MapOfTextures.end())
	{
		//creates a temp texture to store the texture
		IDirect3DTexture9* temp;
		//creates the texture from a file
		D3DXCreateTextureFromFile(gDevice, name.data(), &temp);
		//puts the texture into the map under a given name
		m_MapOfTextures.insert(std::make_pair(name, temp));
		//puts the texture into the vector under the temp.
		m_VectorOfTextures.push_back(temp);
		return true;
	}
	else
	{
		return false;
	}
}

IDirect3DTexture9* Texture_Manager::get_Texture(std::string name)
{
	auto retVal = m_MapOfTextures.find(name);
	if(retVal == m_MapOfTextures.end())
	{
		//creates a temp texture to store the texture
		IDirect3DTexture9* temp;
		//creates the texture from a file
		D3DXCreateTextureFromFile(gDevice, name.data(), &temp);
		//puts the texture into the map under a given name
		m_MapOfTextures.insert(std::make_pair(name, temp));
		//puts the texture into the vector under the temp.
		m_VectorOfTextures.push_back(temp);
		return temp;
	}

	return retVal->second;
}
