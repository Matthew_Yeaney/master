#pragma once
#include "d3dApp.h"

enum button_Show
{
	hovered, not_hovered
};

class Sprite_Base
{
private:
	D3DXVECTOR3 mPosition;
	D3DXVECTOR3 mCenter;
	D3DXVECTOR3 mVelocity;
	D3DXVECTOR3 mArrivePos;
	D3DXVECTOR3 mCurrentPos;

public:
	Sprite_Base(void);
	Sprite_Base(D3DXVECTOR3 Pos, D3DXVECTOR3 Vel, D3DXVECTOR3 Cen);
	~Sprite_Base(void);

	//accessors
	//Position values returned
	D3DXVECTOR3 getPos(){return mPosition;}
	float get_X_Pos(){return mPosition.x;}
	float get_Y_Pos(){return mPosition.y;}
	//Center values returned
	D3DXVECTOR3 getCen(){return mCenter;}
	float get_X_Cen(){return mCenter.x;}
	float get_Y_Cen(){return mCenter.y;}
	//velocity values
	D3DXVECTOR3 getVel(){return mVelocity;}
	float get_X_Vel(){return mVelocity.x;}
	float get_Y_Vel(){return mVelocity.y;}

	//mutators
	//Position values
	void set_Pos(D3DXVECTOR3 Pos){mPosition = Pos;}
	void set_X_Pos(float xVal){mPosition.x = xVal;}
	void set_Y_Pos(float yVal){mPosition.y = yVal;}
	//Velocity values
	void set_Vel(D3DXVECTOR3 Vel){mVelocity = Vel;}
	void set_X_Vel(float xVal){mVelocity.x = xVal;}
	void set_Y_Val(float yVal){mVelocity.y = yVal;}
	//Center values
	void set_Cen(D3DXVECTOR3 Cen){mCenter = Cen;}
	void set_X_Cen(float xVal){mCenter.x = xVal;}
	void set_Y_Cen(float yVal){mCenter.y = yVal;}
	


	virtual void updateScene(float dt) = 0;
	virtual void render(ID3DXSprite *Sprite) = 0;


};

