#pragma once
#include "sprite_base.h"
#include <functional>
#include <string>



struct  Button_Desc
{
	D3DXVECTOR3 Posit, Veloc, Center;
	std::string label, bFont, tString;
	std::function<void(void)> bEffect;
	bool click;
};

class Sprite_Button : public Sprite_Base
{
private:
	RECT Rect;
	std::function< void/*return type of function*/ (void/*Arguments, can have multiple*/)> mButttonEffect; //Lambda c++11
	IDirect3DTexture9* mButtonTex;
	std::string m_label;
	ID3DXFont* m_bFont;
	//std::string m_font_type;
	bool isHovered;
	bool m_Clicked;

public:

	Sprite_Button(void);
	Sprite_Button(D3DXVECTOR3 Pos, D3DXVECTOR3 Vel, D3DXVECTOR3 Cen, std::function<void(void)> bEffect,
		std::string tex_name,
		std::string label,
		std::string fType, bool clickable);

	~Sprite_Button(void);

	void updateScene(float dt);
	void render(ID3DXSprite *Sprite);

	bool OverButton(float X_mouse, float Y_mouse);
	void onClick();

	void resetButton();


};

