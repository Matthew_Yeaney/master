//=============================================================================
// PSystem.h by Frank Luna (C) 2004 All Rights Reserved.
//=============================================================================

#ifndef PSYSTEM_H
#define PSYSTEM_H

#include "d3dUtil.h"
#include "Vertex.h"
#include <vector>
#include "GameCamera.h"


//===============================================================
class PSystem
{
public:
	PSystem(
		const std::string& fxName, 
		const std::string& techName, 
		const std::string& texName, 
		const D3DXVECTOR3& accel, 
		int maxNumParticles,
		float timePerParticle);

	virtual ~PSystem();

	GameCamera gCamera;

	// Access Methods
	float getTime();
	void  setTime(float t);

	void setWorldMtx(const D3DXMATRIX& world);
	void addParticle();

	virtual void onLostDevice();
	virtual void onResetDevice();

	void setVelocity(D3DXVECTOR3 velocity);

	virtual void initParticle(Particle& out) = 0;
	virtual void update(float dt);
	/*virtual*/ void draw();

	/*std::vector<Particle*> getAliveParticle();
	std::vector<Particle*> getDeadParticle();
	void GetParticles(std::vector<Particle*> &who);*/

protected:
	// In practice, some sort of ID3DXEffect and IDirect3DTexture9 manager should
	// be used so that you do not duplicate effects/textures by having several
	// instances of a particle system.
	ID3DXEffect* mFX;
	D3DXHANDLE mhTech;
	D3DXHANDLE mhWVP;
	D3DXHANDLE mhEyePosL;
	D3DXHANDLE mhTex;
	D3DXHANDLE mhTime;
	D3DXHANDLE mhAccel;
	D3DXHANDLE mhViewportHeight;

	IDirect3DTexture9* mTex;
	IDirect3DVertexBuffer9* mVB;
	D3DXMATRIX mWorld;
	D3DXMATRIX mInvWorld;
	float mTime;
	D3DXVECTOR3 mAccel;

	int mMaxNumParticles;
	float mTimePerParticle;

	std::vector<Particle> mParticles;
	std::vector<Particle*> mAliveParticles;
	std::vector<Particle*> mDeadParticles;

};

#endif // P_SYSTEM