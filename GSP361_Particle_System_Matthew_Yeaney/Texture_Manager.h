#pragma once
#include <map>
#include <vector>
#include <string>
#include <d3d9.h>
#include <d3dx9.h>
#include <DxErr.h>



class Texture_Manager
{
private:
	std::vector<IDirect3DTexture9*> m_VectorOfTextures;
	std::map<std::string, IDirect3DTexture9*> m_MapOfTextures;
	IDirect3DDevice9* gDevice;

public:
	Texture_Manager(void);
	Texture_Manager(IDirect3DDevice9* gDDevice);
	~Texture_Manager(void);

	//Registers the Texture
	bool Register_Texture(std::string name);

	//finds the Texture in the map
	IDirect3DTexture9* get_Texture(std::string name);

	


};

