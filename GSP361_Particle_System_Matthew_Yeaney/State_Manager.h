#pragma once
#include <map>
#include <vector>
#include <string>
#include <d3d9.h>
#include <d3dx9.h>
#include <DxErr.h>

class FSM;
class FSMState;

class State_Manager	
{
private:
	std::vector<FSMState*> m_VectorOfStates;
	std::map<std::string, FSMState*> m_MapOfStates;

public:
	State_Manager(void);
	~State_Manager(void);

	FSM *m_pMyFSM;

	//registers the states
	void switchState(std::string);
	
};

