#include "Menu_State.h"
#include "Resource_Manager.h"
#include "Button_Manager.h"
#include "Sprite_Button.h"
#include "d3dApp.h"
#include "State_Manager.h"

Menu_State::Menu_State(void)
{
	//this to fill out the button
	//creates the description for the Demos Button
	Button_Desc tButtonDEsc;
	tButtonDEsc.bFont = "font1";					//the font used for the button
	tButtonDEsc.label = "Particle Display";					//Label that is written on the Button
	tButtonDEsc.Posit = D3DXVECTOR3(400,300,0);		//sets the buttons position on the screen
	tButtonDEsc.Veloc = D3DXVECTOR3(0,0,0);			//sets the initial velocity of the button
	tButtonDEsc.Center = D3DXVECTOR3(64,16,0);		//sets the center of the button for the texture
	tButtonDEsc.tString = "Slider_BackGround.png";	//sets the Texture of the button
	tButtonDEsc.click = false;						//sets the button clicked to false so it can be clicked
	tButtonDEsc.bEffect = [&]()						//Lambda that makes the button work.
	{
		gd3dApp->StateManaged()->switchState("ParticleShow");
	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("MainMenuDemosButton",&tButtonDEsc);
	m_Buttons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("MainMenuDemosButton"));

	//creates the description for the Options button
	tButtonDEsc.label = "Options";
	tButtonDEsc.Posit = D3DXVECTOR3(400,350,0);
	tButtonDEsc.bEffect = [&]()
	{
		//gd3dApp->StateManaged()->switchState("Options");
	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("MainMenuOptionsButton",&tButtonDEsc);
	m_Buttons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("MainMenuOptionsButton"));

	//creates the button for the Quit;
	tButtonDEsc.label = "Quit";
	tButtonDEsc.Posit = D3DXVECTOR3(400,400,0);
	tButtonDEsc.bEffect = [&]()
	{
		exit(1);
	};
	gd3dApp->get_R_Mgr()->get_ButtMgr()->Register_Button("MainMenuQuitButton",&tButtonDEsc);
	m_Buttons.push_back(gd3dApp->get_R_Mgr()->get_ButtMgr()->get_Button("MainMenuQuitButton"));

}

Menu_State::Menu_State(FSM *fsm)
{
	stateName = "MainMenu";
	myFsm = fsm;
}

Menu_State::~Menu_State(void)
{
	/*for(auto it : m_Buttons)
	{
		delete it;
	}*/
}

void Menu_State::onResetDevice()
{	
	//nothing goes in here anymore
}

void Menu_State::onLostDevice()
{
	//nothing goes in here anymore
}

void Menu_State::UpdateScene(float dt)
{	
	// iterate over buttons calling update
	for(auto it : m_Buttons)
	{
		it->updateScene(dt);
	}
}	
	
void Menu_State::RenderScene()
{	
	gd3dApp->get_R_Mgr()->get_Sprite()->Begin(16);
	// iterate over buttons calling render
	for(auto it : m_Buttons)
	{
		it->render(gd3dApp->get_R_Mgr()->get_Sprite());
	}
	gd3dApp->get_R_Mgr()->get_Sprite()->Flush();
}	
	
void Menu_State::InitializeState()
{	
	for(auto butns : m_Buttons)
	{
		butns->resetButton();
	}
}	
	
void Menu_State::LeaveState()
{
	
}
