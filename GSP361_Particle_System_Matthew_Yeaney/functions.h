#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <ctime>

// function to get the sign (+1 or -1) of an integer
inline double sign(double x)
{
    if (x < 0)
		return -1;
    else 
		return 1;
}

const double PI = 3.14159;

inline double dtor(double Deg)
{
	return Deg * (PI/180);
}

inline double rtod(double rad)
{
	return rad * (180/PI);
}

inline double AccelNormalize(double accel)
{
	double temp;
	temp = accel * .001;
}


#endif // FUNCTIONS_H

