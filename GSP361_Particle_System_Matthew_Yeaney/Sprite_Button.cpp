#include "Sprite_Button.h"
#include "d3dApp.h"
#include "Texture_Manager.h"
#include "Resource_Manager.h"
#include "Font_Manager.h"

Sprite_Button::Sprite_Button(void)
{

}

Sprite_Button::Sprite_Button(D3DXVECTOR3 Pos, D3DXVECTOR3 Vel, D3DXVECTOR3 Cen, 
							 std::function<void(void)> bEffect,
							 std::string texture_name,
							 std::string label,
							 std::string fType,
							 bool clickable) : Sprite_Base(Pos, Vel, Cen)
{
	mButtonTex = gd3dApp->get_R_Mgr()->get_TexMgr()->get_Texture(texture_name); //access to the resource manager to get a texture
	m_label = label;
	mButttonEffect = bEffect;
	m_bFont = gd3dApp->get_R_Mgr()->get_FontMgr()->get_Font(fType);
	m_Clicked = clickable;
	isHovered = not_hovered;
}

Sprite_Button::~Sprite_Button(void)
{

}

void Sprite_Button::updateScene(float dt)
{
	// 2d physics based on pos and vel add arrive mechanic if u want
	this->set_Pos(this->getPos() + this->getVel());
	if(!m_Clicked)
	{
		POINT mouse;
		GetCursorPos(&mouse);
		ScreenToClient(gd3dApp->getMainWnd(), &mouse);
		if(OverButton((float)mouse.x, (float)mouse.y))
		{
			isHovered = hovered;
		}
		else
		{
			isHovered = not_hovered;
		}

		onClick();
	}
}

void Sprite_Button::render(ID3DXSprite *Sprite)
{
	//creates a matrix 4x4 for M and T.
	D3DXMATRIX M,T;
	//sets M to the Identity Matrix
	D3DXMatrixIdentity(&M);
	//sets T to the Translation Matrix 4x4;
	D3DXMatrixTranslation(&T, this->get_X_Pos(), this->get_Y_Pos(),0);
	//this will set the TransformMatrix to the Identity Matrix
	Sprite->SetTransform(&M);
	//making the button react if the mouse is over it
	//int s = isHovered;   //finds if the button is being hovered over
	//RECT Rct = {(LONG)(0 + s * 2.0f * get_X_Cen()),0,(LONG)(2.0f * get_X_Cen() + s * 2.0f * get_X_Cen()),(LONG)(2.0f * get_Y_Cen())};
	
	//this will draw the sprite to the GUI
	Sprite->Draw(mButtonTex, 0, &this->getCen(), &this->getPos(), D3DCOLOR_RGBA(0,255,0,255)/*0xFF008000*/);
	//sets the transform Matrix to the T matrix which has been translated
	Sprite->SetTransform(&T);
	//create the RECT
	RECT rect = {0,0,0,0};
	//Create the text on the Button
	m_bFont->DrawTextA(Sprite, m_label.data(), -1, &rect, DT_CENTER|DT_VCENTER |DT_CALCRECT, D3DCOLOR_RGBA(255,0,255,0));
	m_bFont->DrawTextA(Sprite, m_label.data(), -1, &rect, DT_CENTER|DT_VCENTER, D3DCOLOR_RGBA(255,0,255,255));

	gd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, false);
	
}

bool Sprite_Button::OverButton(float X_mouse, float Y_mouse)
{
	if(this->get_X_Pos() - this->get_X_Cen() > X_mouse)
	{
		return false;
	}
	if(this->get_X_Pos() + this->get_X_Cen() < X_mouse)
	{
		return false;
	}
	if(this->get_Y_Pos() - this->get_Y_Cen() > Y_mouse)
	{
		return false;
	}
	if(this->get_Y_Pos() + this->get_Y_Cen() < Y_mouse)
	{
		return false;
	}
	else
	{
		return true;
	}
}

void Sprite_Button::onClick()
{
	if(!m_Clicked)
	{
		POINT mouse;
		GetCursorPos(&mouse);
		ScreenToClient(gd3dApp->getMainWnd(), &mouse);
		if(OverButton((float)mouse.x, (float)mouse.y))
		{
			if(gd3dApp->LMB_is_Clicked() == true)
			{
				this->mButttonEffect();			
			}
		}
	}
}

//resets the button pushed to false so it can be clicked again
void Sprite_Button::resetButton()
{
	if(m_Clicked == true)
	{
		m_Clicked = false;
	}
}
