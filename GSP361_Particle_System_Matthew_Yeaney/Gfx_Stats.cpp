#include "Gfx_Stats.h"

#include "d3dUtil.h"
#include "d3dApp.h"
#include "Gfx_Stats.h"
#include <tchar.h>
#include "Resource_Manager.h"
#include "Font_Manager.h"

Gfx_Stats::Gfx_Stats()
: mFont(0), mFPS(0.0f), mMilliSecPerFrame(0.0f), mNumTris(0), mNumVertices(0)
{
	D3DXFONT_DESC fontDesc;
	fontDesc.Height          = 18;
    fontDesc.Width           = 0;
    fontDesc.Weight          = 0;
    fontDesc.MipLevels       = 1;
    fontDesc.Italic          = false;
    fontDesc.CharSet         = DEFAULT_CHARSET;
    fontDesc.OutputPrecision = OUT_DEFAULT_PRECIS;
    fontDesc.Quality         = DEFAULT_QUALITY;
    fontDesc.PitchAndFamily  = DEFAULT_PITCH | FF_DONTCARE;
    _tcscpy_s(fontDesc.FaceName, _T("Times New Roman"));

	//HR(D3DXCreateFontIndirect(gd3dDevice, &fontDesc, &mFont));
	gd3dApp->get_R_Mgr()->get_FontMgr()->Register_Font("font1", fontDesc);

	mFont = gd3dApp->get_R_Mgr()->get_FontMgr()->get_Font("font1");

}

Gfx_Stats::~Gfx_Stats()
{
	//ReleaseCOM(mFont);
}

void Gfx_Stats::onLostDevice()
{
	//HR(mFont->OnLostDevice());
}

void Gfx_Stats::onResetDevice()
{
	//HR(mFont->OnResetDevice());
}

void Gfx_Stats::addVertices(DWORD n)
{
	mNumVertices += n;
}

void Gfx_Stats::subVertices(DWORD n)
{
	mNumVertices -= n;
}

void Gfx_Stats::addTriangles(DWORD n)
{
	mNumTris += n;
}

void Gfx_Stats::subTriangles(DWORD n)
{
	mNumTris -= n;
}

void Gfx_Stats::setTriCount(DWORD n)
{
	mNumTris = n;
}

void Gfx_Stats::setVertexCount(DWORD n)
{
	mNumVertices = n;
}

void Gfx_Stats::update(float dt)
{
	// Make static so that their values persist accross function calls.
	static float numFrames   = 0.0f;
	static float timeElapsed = 0.0f;

	// Increment the frame count.
	numFrames += 1.0f;

	// Accumulate how much time has passed.
	timeElapsed += dt;

	// Has one second passed?--we compute the frame statistics once 
	// per second.  Note that the time between frames can vary so 
	// these stats are averages over a second.
	if( timeElapsed >= 1.0f )
	{
		// Frames Per Second = numFrames / timeElapsed,
		// but timeElapsed approx. equals 1.0, so 
		// frames per second = numFrames.

		mFPS = numFrames;

		// Average time, in miliseconds, it took to render a single frame.
		mMilliSecPerFrame = 1000.0f / mFPS;

		// Reset time counter and frame count to prepare for computing
		// the average stats over the next second.
		timeElapsed = 0.0f;
		numFrames   = 0.0f;
	}
}

void Gfx_Stats::display()
{
	// Make static so memory is not allocated every frame.
	static char buffer[256];

	sprintf_s(buffer, "Frames Per Second = %.2f\n"
		"Milliseconds Per Frame = %.4f\n"
		"Particle Count = %d\n"		//Easy sauce
		"Vertex Count = %d", mFPS, mMilliSecPerFrame, mNumTris, mNumVertices);

	RECT R = {5, 5, 0, 0};
	HR(mFont->DrawText(0, buffer, -1, &R, DT_NOCLIP, D3DCOLOR_XRGB(0,255,0)));
}