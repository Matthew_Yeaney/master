#include "Button_Manager.h"
#include "Sprite_Button.h"
#include "Sprite_Slider.h"

Button_Manager::Button_Manager(void)
{

}

Button_Manager::~Button_Manager(void)
{
	for(auto it : m_VectorOfButtons)
	{
		delete it;
	}
	for(auto ij : m_VectorOfSliders)
	{
		delete ij;
	}
}

bool Button_Manager::Register_Button(std::string name, Button_Desc* buttonDescStruct)
{
	auto retVal = m_MapOfButtons.find(name);
	if(retVal == m_MapOfButtons.end())
	{
		Sprite_Button* temp = new Sprite_Button(buttonDescStruct->Posit,buttonDescStruct->Veloc, buttonDescStruct->Center,
			buttonDescStruct->bEffect, buttonDescStruct->tString, buttonDescStruct->label, buttonDescStruct->bFont, buttonDescStruct->click);
		m_MapOfButtons.insert(std::make_pair(name, temp));
		m_VectorOfButtons.push_back(temp);
		return true;
	}
	else
	{
		return false;
	}
}

bool Button_Manager::Register_Slider(std::string name, Slider_Desc* SliderDescStruct)
{
	auto retVal = m_MapOfButtons.find(name);
	if(retVal == m_MapOfButtons.end())
	{
		Sprite_Slider* temp = new Sprite_Slider(SliderDescStruct->m_sPosit, SliderDescStruct->m_sVeloc, SliderDescStruct->m_sCenter, SliderDescStruct->sPos,
			SliderDescStruct->sEffect, SliderDescStruct->backImage, SliderDescStruct->foreImage, 0, 1, SliderDescStruct->SliderVal, SliderDescStruct->click);
		m_MapOfSliders.insert(std::make_pair(name, temp));
		m_VectorOfSliders.push_back(temp);
		return true;
	}
	else
	{
		return false;
	}
}


//searches the map of buttons for the button
Sprite_Button* Button_Manager::get_Button(std::string name)
{
	auto retVal = m_MapOfButtons.find(name);
	if(retVal == m_MapOfButtons.end())
	{
		return nullptr;
	}
	return retVal->second;
}

Sprite_Slider* Button_Manager::get_Slider(std::string name)
{
	auto retVal = m_MapOfSliders.find(name);
	if(retVal == m_MapOfSliders.end())
	{
		return nullptr;
	}
	return retVal->second;
}

