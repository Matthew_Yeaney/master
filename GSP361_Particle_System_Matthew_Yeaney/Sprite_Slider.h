#pragma once
#include "Sprite_Base.h"
#include <functional>
#include <string>

struct Slider_Desc
{
	D3DXVECTOR3 m_sPosit, m_sVeloc, m_sCenter, sPos;
	std::string backImage, foreImage;
	std::function<void(void)> sEffect;
	double minX, maxX, SliderVal;
	bool click;
};

class Sprite_Slider : public Sprite_Base
{
private:
	RECT m_sRect;
	std::function<void(void)> mSliderEffect;
	IDirect3DTexture9* mBackTexture;
	IDirect3DTexture9* mForeTexture;
	D3DXVECTOR3 m_Slider_Pos;
	double min_X;
	double max_X;
	double s_Val;
	bool m_Clicked;

public:
	Sprite_Slider(void);
	Sprite_Slider(D3DXVECTOR3 Pos, D3DXVECTOR3 Vel, D3DXVECTOR3 Cen,
		D3DXVECTOR3 sPosition,
		std::function<void(void)> sEffect,
		std::string tex_name1,
		std::string tex_name2,
		double min_X, double max_X, double SliderVal,
		bool clickable);
	~Sprite_Slider(void);

	void updateScene(float dt);
	void render(ID3DXSprite *Sprite);

	//accessors
	double get_Min_X(){return min_X;}
	double get_Max_X(){return max_X;}
	double get_S_Pos(){return s_Val;}
	D3DXVECTOR3 get_Slider_Pos(){return m_Slider_Pos;}
	double get_S_Val(){return s_Val;}

	//mutators
	void set_S_Val(double sVal){s_Val = sVal;}
	void set_Slider_Pos(D3DXVECTOR3 Val){m_Slider_Pos = Val;}

	//methods
	void setUpVal();

	bool OverButton(float X_mouse, float Y_mouse);
	void onClick();

	void resetButton();

};

