#include "Font_Manager.h"
#include "d3dApp.h"


Font_Manager::Font_Manager(void)
{

}

Font_Manager::~Font_Manager(void)
{
	for(auto it : m_VectorOfFonts)
	{
		ReleaseCOM(it);
	}
}

bool Font_Manager::Register_Font(std::string name, D3DXFONT_DESC Font)
{
	
	auto retVal = m_MapOfFonts.find(name);					//searches for the name in a font_map first
	if(retVal == m_MapOfFonts.end())						// if cant find it, then implements it into the map
	{
		ID3DXFont* temp;									//creates a temp font so it can be creates.
		D3DXCreateFontIndirect(gd3dDevice, &Font, &temp);	//creates the font using the font desc
		m_MapOfFonts.insert(std::make_pair(name, temp));	// inserts the font into the map under a given name
		m_VectorOfFonts.push_back(temp);					// puts the font into the vector of fonts.
		return true;
	}
	else
	{

		return false;										// returns false if cant implement into the map
	}

}

ID3DXFont* Font_Manager::get_Font(std::string name)
{
	auto retVal = m_MapOfFonts.find(name);					// searches for the name in the map
	if(retVal == m_MapOfFonts.end())						// if cant find the name, then returns a null pointer
	{
		return nullptr;										// null pointer
	}
	//returns the font,  the ->second means the second part of the map input or the Font
	return retVal->second;
}

void Font_Manager::onLostDevice()
{
	//loops through the vector and calls the OnLostDevice()
	for(auto it : m_VectorOfFonts)
	{
		it->OnLostDevice();
	}
}

void Font_Manager::onResetDevice()
{
	//loops through the vecotr and calls the OnResetDevice()
	for(auto it : m_VectorOfFonts)
	{
		it->OnResetDevice();
	}
}
