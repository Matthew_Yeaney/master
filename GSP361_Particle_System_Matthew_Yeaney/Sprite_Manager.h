#pragma once

#include <vector>
#include "Sprite_Class.h"


class Sprite_Manager
{
private:
	Sprite_Manager(void);
	~Sprite_Manager(void);


public:
	

	//methods
	void Register_Sprite(Sprite_Class *);


	void onResetDevice();
	void onLostDevice();

};

