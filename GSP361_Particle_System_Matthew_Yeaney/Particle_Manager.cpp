#include "Particle_Manager.h"
#include "PSprinkler.h"
#include "d3dApp.h"
#include "Resource_Manager.h"


Particle_Manager::Particle_Manager(void)
{
	
}

Particle_Manager::~Particle_Manager(void)
{
	for(auto it : m_Vector_Of_Sprinkler_Systems)
	{
		delete it;
	}
}

void Particle_Manager::Add_Particle(std::string name, D3DXVECTOR3 Veloc, D3DXVECTOR3 WindDir, D3DXVECTOR3 Pos, float NumParticle, float PPS, float Life, float Size, float mass, int tColor)
{
	auto retVal = m_Map_Of_Particle_Systems.find(name);					
	if(retVal == m_Map_Of_Particle_Systems.end())						
	{

	}

	if(name == "Sprinkler")
	{
		D3DXMATRIX psysWorld;
		D3DXMatrixIdentity(&psysWorld);

		PSprinkler* temp = new PSprinkler("sprinkler.fx", "SprinklerTech", "bolt.dds", 
			WindDir, Pos, NumParticle, PPS);

		temp->set_e_LifeTime(Life);
		temp->Set_e_Time(PPS);
		temp->Set_e_Size(Size);
		temp->SetXVeloc(Veloc.x);
		temp->setYVeloc(Veloc.y);
		temp->setZVeloc(Veloc.z);
		temp->set_e_Mass(mass);
		switch(tColor)
				{
				case 1:
					temp->set_e_Color(WHITE);
					break;
				case 2:
					temp->set_e_Color(GREY);
					break;
				case 3:
					temp->set_e_Color(RED);
					break;
				case 4:
					temp->set_e_Color(GREEN);
					break;
				case 5:
					temp->set_e_Color(BLUE);
					break;
				case 6:
					temp->set_e_Color(YELLOW);
					break;
				case 7:
					temp->set_e_Color(CYAN);
					break;
				case 8:
					temp->set_e_Color(MAGENTA);
					break;
				case 9:
					temp->set_e_Color(LBLUE);
					break;
				case 10:
					temp->set_e_Color(LIMEG);
					break;
				default:
					break;
				};

		temp->setWorldMtx(psysWorld);

		m_Map_Of_Particle_Systems.insert(std::make_pair(name, temp));
		m_Vector_Of_Sprinkler_Systems.push_back(temp);
	}

	/*if(name == "Firework1")
	{

	}*/

}

PSystem* Particle_Manager::Get_System(std::string name)
{
	auto retVal = m_Map_Of_Particle_Systems.find(name);
	if(retVal == m_Map_Of_Particle_Systems.end())
	{
		return nullptr;
	}
	return retVal->second;

}

void Particle_Manager::onLostDevice()
{
	for( auto it : m_Vector_Of_Sprinkler_Systems)
	{
		it->onLostDevice();
	}

}

void Particle_Manager::onResetDevice()
{
	for(auto it : m_Vector_Of_Sprinkler_Systems)
	{
		it->onResetDevice();	
	}

}

void Particle_Manager::UpDate(float dt)
{
	for(auto it : m_Vector_Of_Sprinkler_Systems)
	{
		it->update(dt);
	}
}

void Particle_Manager::ResetVector()
{
		m_Vector_Of_Sprinkler_Systems.resize(0);

}

void Particle_Manager::Draw()
{

	//will draw all the Particle Systems
	for(auto it : m_Vector_Of_Sprinkler_Systems)
	{
		it->draw();
	}
}

