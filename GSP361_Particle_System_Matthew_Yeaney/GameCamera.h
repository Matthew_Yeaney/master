#pragma once

#include <d3dx9.h>

class DirectInput;

class GameCamera
{
private:
	D3DXMATRIX mWorld, mProject;
	D3DXVECTOR3 Position, Target, Up, Forward, Right;
	float Speed, Sensitivity;

public:
	GameCamera(void);
	~GameCamera(void);

	bool Initialize(D3DXVECTOR3 position, D3DXVECTOR3 target, HWND hWnd);
	void Update(float dt, DirectInput* dInput);
	void BuildViewMatrix();
	void BuildProjMatrix(HWND hWnd);
	void Rotate(float dt, DirectInput* dInput);
	void Adjust(float dt, DirectInput* dInput);
	void SetPosition(D3DXVECTOR3 pos);
	void SetTarget(D3DXVECTOR3 target);

	D3DXMATRIX GetViewMatrix(){return mWorld;};
	D3DXMATRIX GetProjectionMatrix(){return mProject;};
	D3DXVECTOR3 GetPosition(){return Position;};
	D3DXMATRIX viewProj(){return mWorld * mProject;};
};